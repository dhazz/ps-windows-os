﻿function Reset-WirelessAdapter
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$false,
                   Position=0)]
        #$AdapterName = "Intel(R) Dual Band Wireless-AC 8260"
	$AdapterName = "Wireless-AC"
    )

    Begin
    {
    }
    Process
    {
        $Adapter = Get-WmiObject -Class Win32_NetworkAdapter | where { $_.Name.Contains($AdapterName) }
        $Adapter.Disable()
        Sleep(8)
        $Adapter.Enable()
    }
    End
    {
    }
}