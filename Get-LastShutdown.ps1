<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>
function Get-LastShutdown
{
    [CmdletBinding()]
    Param
    (
        # Computer Name
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [String]$ComputerName = $env:COMPUTERNAME,

        # Default number of events to return
        [Parameter(Mandatory=$false)]
        [int]$NumberOfEvents = 10
    )

    Begin
    {
    }
    Process
    {
        #foreach ($item in $collection)
        #{
            Get-WinEvent -ComputerName $ComputerName -FilterHashtable @{logname='System'; id=1074;}  | ForEach-Object {
                $rv = New-Object PSObject | Select-Object Date, User, Action, Process, Reason, ReasonCode, Comment
                $rv.Date = $_.TimeCreated
                $rv.User = $_.Properties[6].Value
                $rv.Process = $_.Properties[0].Value
                $rv.Action = $_.Properties[4].Value
                $rv.Reason = $_.Properties[2].Value
                $rv.ReasonCode = $_.Properties[3].Value
                $rv.Comment = $_.Properties[5].Value
                $rv
            } | Select -First $NumberOfEvents
            #| Select-Object Date, Action, Reason, User -First $NumberOfEvents
        #}
    }
    End
    {
    }
}


