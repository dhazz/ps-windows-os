<#
.Synopsis
    Short description
.DESCRIPTION
    Long description
.EXAMPLE
    Example of how to use this cmdlet
.EXAMPLE
    Another example of how to use this cmdlet
#>

function Format-ByteToHumanReadable($Size)
{
    switch ($Size) {
        { $_ -ge 1PB } { "{0:#.#'PB' }" -f ($Size / 1PB); break }
        { $_ -ge 1TB } { "{0:#.#'TB' }" -f ($Size / 1TB); break }
        { $_ -ge 1GB } { "{0:#.#'GB' }" -f ($Size / 1GB); break }
        { $_ -ge 1MB } { "{0:#.#'MB' }" -f ($Size / 1MB); break }
        { $_ -ge 1KB } { "{0:#'KB' }" -f ($Size / 1KB); break }
        default {"{0}" -f ($Size) + "B"}
    }
}

function Get-DiskUsage
{
    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [string[]]$ComputerName
    )

    Begin
    {
    }
    Process
    {
        $Object = @();

        foreach ($Computer in $ComputerName)
        {
            if ((Test-Connection -ComputerName $ComputerName -Count 1 -Quiet) -eq $true)
            {
                $Query = "SELECT * FROM Win32_LogicalDisk WHERE ";         
                $LogicalDisk = Get-WmiObject -Class Win32_LogicalDisk -ComputerName $Computer -Filter "DriveType = 3";

                foreach($Disk in $LogicalDisk)
                {
                    $Params = [Ordered]@{'DeviceID'     = $Disk.DeviceID;
                                         'FileSystem'   = $Disk.FileSystem;
                                         'VolumeName'   = $Disk.VolumeName;
                                         'Size'         = Format-ByteToHumanReadable($Disk.Size);
                                         'FreeSpace'    = Format-ByteToHumanReadable($Disk.FreeSpace);
                                         'Used'         = Format-ByteToHumanReadable($Disk.Size - $Disk.FreeSpace);
                                         'PctFreeSpace' = ($Disk.FreeSpace / $Disk.Size).toString("P");
                                        };
                    $Object += New-Object -TypeName PSObject -Property $Params

                }
            }
            else
            {
                Write-Error "Cannot connect to $($Computer)";
            }
        }
    }
    End
    {
        Write-Output $Object | Format-Table -AutoSize;
    }
}
#Get-DiskUsage usoktulnas02