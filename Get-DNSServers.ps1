﻿<#
.Synopsis
   Set DNS Servers on remote server via WMI
.DESCRIPTION
   Set DNS Servers on remote server via WMI
.EXAMPLE
   Set-DNSServers -Computer servername -Primary 1.1.1.1 -Secondary 2.2.2.2
.EXAMPLE
   "Computer1","Computer2" | Set-DNSServers -Primary 1.1.1.1 -Secondary 2.2.2.2
.INPUTS
   String
.OUTPUTS
   None
.Notes 
    NAME: Get-DNSServers
    AUTHOR: dhazz
    LASTEDIT: 05/19/2015
#>
function Get-DNSServers
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                   Position=0,
                   ValueFromPipeline=$true,
                   HelpMessage="Enter target computer")]
        [String[]]$ComputerName
    )

    Begin
    {
        $Results = @()
    }
    Process
    {
        foreach($ComputerItem in $ComputerName)
        {
            $Parameters = [Ordered]@{'Computer'  = $ComputerItem
                                     'Primary'   = $null
                                     'Secondary' = $null
                                    }
            $Query = Get-WmiObject -Class Win32_NetworkAdapterConfiguration -ComputerName $ComputerItem -Filter "IPEnabled = 'TRUE'"
            
            if($Query -ne $null)
            {
                Write-Verbose "Querying $ComputerItem"
                Write-Verbose "DNSHostName is $($Query.DNSHostName)"
                
                foreach($Item in $Query.DNSServerSearchOrder)
                {
                    if($Query.DNSServerSearchOrder.IndexOf($Item) -eq 0)
                    {
                        $Parameters.Primary = $Item
                    }

                    if($Query.DNSServerSearchOrder.IndexOf($Item) -eq 1)
                    {
                        $Parameters.Secondary = $Item
                    }
                }
            }
            else
            {
                Write-Error "No wmi value returned for $($ComputerItem)"
            }

            $Results += New-Object PSObject -Property $Parameters
        }
    }
    End
    {
        $Results
    }
}

